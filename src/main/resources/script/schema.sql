-- Create table product
CREATE TABLE product_tb(
    product_id SERIAL PRIMARY KEY,
    product_name varchar(50) not null ,
    product_price float4 not null
);
-- Insert value into table
INSERT INTO product_tb (product_name, product_price) values ('Coke',1.5),
                                                            ('Pepsi',2),
                                                            ('Spy',1),
                                                            ('Water',1);

SELECT * FROM product_tb;

DROP TABLE product_tb;

CREATE TABLE invoice_tb(
    invoice_id SERIAL PRIMARY KEY,
    invoice_date timestamp DEFAULT(current_timestamp),
    customer_id int REFERENCES customer_tb (customer_id) ON UPDATE CASCADE ON DELETE CASCADE
);
-- Insert into Value inovice_tb
INSERT INTO invoice_tb (customer_id) VALUES (1),(3),(4),(2),(3);

DROP TABLE invoice_tb;

SELECT * FROM invoice_tb;


CREATE TABLE invoice_detail_tb (
  id SERIAL PRIMARY KEY ,
  invoice_id INTEGER REFERENCES invoice_tb (invoice_id) ON UPDATE CASCADE ON DELETE CASCADE,
  product_id INTEGER REFERENCES product_tb(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);


SELECT * FROM invoice_detail_tb;

INSERT INTO invoice_detail_tb (invoice_id, product_id) VALUES (1,1),(2,3),(3,4),(5,2);


CREATE TABLE customer_tb(
    customer_id SERIAL PRIMARY KEY ,
    customer_name varchar(50) not null ,
    customer_address varchar(255) not null,
--      customer_phone INTEGER NOT NULL
    customer_phone varchar(255) NOT NULL
);
-- INSERT into table customer
INSERT INTO customer_tb (customer_name, customer_address, customer_phone) VALUES
                                                                              ('Ming','PP',012234567),
                                                                              ('Ling','KPS',01123423),
                                                                              ('Rin','KPC',099876344),
                                                                              ('Meng','SR',081323244);

SELECT * FROM customer_tb;

DELETE FROM customer_tb WHERE customer_id > 5;


DROP TABLE customer_tb;

INSERT INTO customer_tb(customer_name, customer_address, customer_phone) VALUES ('Mi','PP',0123455) returning customer_id;


UPDATE customer_tb SET customer_name = 'Ki',
             customer_address = 'PP',
             customer_phone = 012345
            WHERE customer_id = 39 returning customer_id;


SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb i
             INNER JOIN product_tb p ON p.product_id = i.product_id
            WHERE invoice_id = 1;

-- INSERT INTO invoice_detail_tb (invoice_id, product_id) VALUES ();

INSERT INTO invoice_tb (invoice_date, customer_id)
VALUES ('12-6-2023',1)
             RETURNING invoice_id;

UPDATE invoice_tb SET customer_id = 1 WHERE invoice_id = 2 returning invoice_id;

UPDATE invoice_detail_tb SET product_id = 2 WHERE invoice_id = 7;


select * from invoice_tb;
SELECT * FROM  invoice_detail_tb;

DELETE FROM invoice_tb WHERE invoice_id = 11;


UPDATE product_tb SET product_name = 'Mi',
            product_price = 5.5
            WHERE product_id = 6 returning product_id;

select * from  product_tb;
