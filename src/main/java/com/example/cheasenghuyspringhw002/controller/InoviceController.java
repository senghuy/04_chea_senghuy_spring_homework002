package com.example.cheasenghuyspringhw002.controller;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.entity.Invoice;
import com.example.cheasenghuyspringhw002.model.request.RequestInvoice;
import com.example.cheasenghuyspringhw002.model.response.CustomerResponse;
import com.example.cheasenghuyspringhw002.model.response.InvoiceResponse;
import com.example.cheasenghuyspringhw002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/inovices")
public class InoviceController {
    private final InvoiceService invoiceService;

    public InoviceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all Inovice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInovices(){
        InvoiceResponse<List<Invoice>> response = null;
        if(invoiceService.getAllInvoice() != null){
            response = InvoiceResponse.<List<Invoice>>builder()
                    .payload(invoiceService.getAllInvoice())
                    .message("Found the Data")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else
        {
            response = InvoiceResponse.<List<Invoice>>builder()
                            .message("Faild to get all Data")
                            .success(false)
                            .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get Invoice by ID ")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById (@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        if(invoiceService.getInvoiceById(invoiceId) != null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .message("ID is Found")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("ID is not found")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/saveInovice")
    @Operation(summary = "add new Invoice")
    public ResponseEntity<?> addNewInvoice(@RequestBody RequestInvoice requestInvoice) {
        InvoiceResponse<Invoice> response = null;
        Integer storeInoviceId = invoiceService.addNewInvoice(requestInvoice);
        if(storeInoviceId !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInoviceId))
                    .message("Insert Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            response = InvoiceResponse.<Invoice>builder()
                    .message("Failed to insert Data")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("/EditInvoice/{id}")
    @Operation(summary = "Update Invoice")
    public ResponseEntity<?> updateInvoiceByID(@RequestBody RequestInvoice requestInvoice,
                                               @PathVariable("id") Integer invoiceId)
    {
        InvoiceResponse<Invoice> response = null;
        Integer storeInoviceId = invoiceService.updateInvoiceById(requestInvoice,invoiceId);
        if(storeInoviceId !=null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInoviceId))
                    .message("Update Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            response = InvoiceResponse.<Invoice>builder()
                    .message("Failed to Update")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete Invoice By ID ")
    public ResponseEntity<?> deleteInvoiceByID(@PathVariable("id") Integer invoiceId)
    {
        InvoiceResponse<Invoice> response = null;
        if(invoiceService.deleteInvoiceById(invoiceId)){
            response = InvoiceResponse.<Invoice>builder()
                    .message("Delete Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = InvoiceResponse.<Invoice>builder()
                    .message("Failed to Delete")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
