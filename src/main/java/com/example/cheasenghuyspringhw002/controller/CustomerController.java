package com.example.cheasenghuyspringhw002.controller;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import com.example.cheasenghuyspringhw002.model.response.CustomerResponse;
import com.example.cheasenghuyspringhw002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/ListCustomers")
    @Operation(summary = "Get all data in Customers")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustoomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.getAllCustomers())
                .message("Found the Data")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
}

@GetMapping("/getCustomerbyID/{id}")
    @Operation(summary = "find customer by id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerByID(@PathVariable("id") Integer customerID){
        CustomerResponse <Customer> response = null;
            if(customerService.getCustomerById(customerID) != null){
                response = CustomerResponse.<Customer>builder()
                        .payload(customerService.getCustomerById(customerID))
                        .message("ID is found")
                        .success(true)
                        .build();
            return ResponseEntity.ok(response);
            }else {
                response = CustomerResponse.<Customer>builder()
                        .message("ID is not found")
                        .success(false)
                        .build();
                return ResponseEntity.badRequest().body(response);
            }
}

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete customer by id")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerByID(@PathVariable("id") Integer customerid){
        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerById(customerid)){
            response = CustomerResponse.<String>builder()
                    .message("Delete Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = CustomerResponse.<String>builder()
                    .message("Failed to delete")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }

    @PostMapping("/add-new-customer")
    @Operation(summary = "Add new Customer")
    public ResponseEntity<CustomerResponse<Customer>> addCustomer(@RequestBody RequestCustomer requestCustomer){
        CustomerResponse<Customer> response = null;
        Integer customerSaveId = customerService.addNewCustomer(requestCustomer);
        if(customerSaveId !=null){
             response = CustomerResponse.<Customer>builder()
                     .payload(customerService.getCustomerById(customerSaveId))
                     .message("Saved Success")
                     .success(true)
                     .build();
             return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<Customer>builder()
                    .message("can not saved")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("update/{id}")
    @Operation(summary = "Update Customer by ID")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(
            @RequestBody RequestCustomer requestCustomer, @PathVariable("id") Integer customerId
    ){
        CustomerResponse<Customer> response =null;
        Integer customerIdUpdate = customerService.updateCustomer(requestCustomer,customerId);
        if(customerIdUpdate !=null){
            response = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customerId))
                    .message("Update Sccuess")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else
        {
            response = CustomerResponse.<Customer>builder()
                    .message("Failed to Update")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
