package com.example.cheasenghuyspringhw002.controller;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.entity.Product;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import com.example.cheasenghuyspringhw002.model.request.RequestProduct;
import com.example.cheasenghuyspringhw002.model.response.CustomerResponse;
import com.example.cheasenghuyspringhw002.model.response.ProductResponse;
import com.example.cheasenghuyspringhw002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/prodcuts")
public class ProductController {
 private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/productlist")
    @Operation(summary = "Get all data in products")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProducts(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProducts())
                .message("Found the Data")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete Product by id")
    public ResponseEntity<ProductResponse<String>> deleteCustomerByID(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if(productService.deleteProductById(productId)){
            response = ProductResponse.<String>builder()
                    .message("Delete Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<String>builder()
                    .message("ID is not Found")
                    .success(true)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }


    @GetMapping("/{id}")
    @Operation(summary = "Get Product by ID")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id")Integer productId){
        ProductResponse<Product> response = null;
        if(productService.getProductById(productId)!=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("String")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Product>builder()
                    .payload(null)
                    .message("String")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/add-product")
    @Operation(summary = "Add new Product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody RequestProduct requestProduct){
        ProductResponse<Product> response = null;
        Integer productSaveId = productService.addNewProduct(requestProduct);
        if(productSaveId !=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productSaveId))
                    .message("Saved")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<Product>builder()
                    .message("can not saved")
                    .success(true)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("update/{id}")
    @Operation(summary = "Update Product by ID")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody RequestProduct requestProduct, @PathVariable("id") Integer productId
    ){
        ProductResponse<Product> response =null;
        Integer productIdUpdate = productService.updateProduct(requestProduct,productId);
        if(productIdUpdate !=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productIdUpdate))
                    .message("Update Success")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else
        {
            response = ProductResponse.<Product>builder()
                    .message("Update Failed")
                    .success(true)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }
}
