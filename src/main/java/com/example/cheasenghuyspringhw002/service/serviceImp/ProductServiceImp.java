package com.example.cheasenghuyspringhw002.service.serviceImp;

import com.example.cheasenghuyspringhw002.model.entity.Product;
import com.example.cheasenghuyspringhw002.model.request.RequestProduct;
import com.example.cheasenghuyspringhw002.repository.ProductRepository;
import com.example.cheasenghuyspringhw002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductByID(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductByID(productId);
    }

    @Override
    public Integer addNewProduct(RequestProduct requestProduct) {
        return productRepository.saveProduct(requestProduct);
    }

    @Override
    public Integer updateProduct(RequestProduct requestProduct, Integer productId) {
        return productRepository.updateProduct(requestProduct,productId);
    }

}
