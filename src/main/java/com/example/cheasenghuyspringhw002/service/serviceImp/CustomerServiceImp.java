package com.example.cheasenghuyspringhw002.service.serviceImp;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import com.example.cheasenghuyspringhw002.repository.CustomerRepository;
import com.example.cheasenghuyspringhw002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerByID(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerByID(customerId);
    }

    @Override
    public Integer addNewCustomer(RequestCustomer requestCustomer) {
        Integer customerId = customerRepository.saveCustomer(requestCustomer);
        return customerId;
    }

    @Override
    public Integer updateCustomer(RequestCustomer requestCustomer, Integer customerId) {
       Integer storeCustomerId = customerRepository.updateCustomer(requestCustomer,customerId);
        return storeCustomerId;
    }
}
