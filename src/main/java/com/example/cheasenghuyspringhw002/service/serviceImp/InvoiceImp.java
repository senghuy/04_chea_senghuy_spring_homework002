package com.example.cheasenghuyspringhw002.service.serviceImp;

import com.example.cheasenghuyspringhw002.model.entity.Invoice;
import com.example.cheasenghuyspringhw002.model.request.RequestInvoice;
import com.example.cheasenghuyspringhw002.repository.InvoiceRepository;
import com.example.cheasenghuyspringhw002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInovice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(RequestInvoice requestInvoice) {
        Integer storeInoviceId = invoiceRepository.savedNewInovice(requestInvoice);

        for (Integer productId : requestInvoice.getProductsIds())
        {
            invoiceRepository.saveInvoiceByProductId(storeInoviceId,productId);
        }
        return storeInoviceId;
    }

    @Override
    public Integer updateInvoiceById(RequestInvoice requestInvoice,Integer invoiceId) {
        Integer storeInoviceId = invoiceRepository.updateInvoiceInvoice(requestInvoice,invoiceId);
        invoiceRepository.deleteInvoiceInInvoiceDetail(invoiceId);
        for (Integer productId : requestInvoice.getProductsIds())
        {
            invoiceRepository.saveInvoiceByProductId(storeInoviceId,productId);
        }
        return storeInoviceId;
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }
}
