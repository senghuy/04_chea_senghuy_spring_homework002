package com.example.cheasenghuyspringhw002.service;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomers();
    Customer getCustomerById(Integer customerId);
    boolean deleteCustomerById(Integer customerId);
    Integer addNewCustomer(RequestCustomer requestCustomer);

    Integer updateCustomer(RequestCustomer requestCustomer, Integer customerId);
}
