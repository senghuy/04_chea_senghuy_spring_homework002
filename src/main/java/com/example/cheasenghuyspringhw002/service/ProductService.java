package com.example.cheasenghuyspringhw002.service;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.entity.Product;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import com.example.cheasenghuyspringhw002.model.request.RequestProduct;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();
    Product getProductById(Integer prodcutId);
    boolean deleteProductById(Integer prodcutId);
    Integer addNewProduct(RequestProduct requestProduct);

    Integer updateProduct(RequestProduct requestProduct, Integer productId);
}
