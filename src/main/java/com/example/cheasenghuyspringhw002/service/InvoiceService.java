package com.example.cheasenghuyspringhw002.service;

import com.example.cheasenghuyspringhw002.model.entity.Invoice;
import com.example.cheasenghuyspringhw002.model.request.RequestInvoice;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();
    Invoice getInvoiceById(Integer invoiceId);
    Integer addNewInvoice(RequestInvoice requestInvoice);
    Integer updateInvoiceById(RequestInvoice requestInvoice,Integer invoiceId);
    boolean deleteInvoiceById(Integer invoiceId);


}
