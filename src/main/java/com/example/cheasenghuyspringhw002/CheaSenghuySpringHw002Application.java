package com.example.cheasenghuyspringhw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheaSenghuySpringHw002Application {

    public static void main(String[] args) {
        SpringApplication.run(CheaSenghuySpringHw002Application.class, args);
    }

}
