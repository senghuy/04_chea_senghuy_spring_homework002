package com.example.cheasenghuyspringhw002.repository;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.entity.Product;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import com.example.cheasenghuyspringhw002.model.request.RequestProduct;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface ProductRepository {
    @Select("SELECT * FROM product_tb")
    List<Product> findAllProduct();

    @Select("SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb i" +
            " INNER JOIN product_tb p ON p.product_id = i.product_id" +
            " WHERE i.invoice_id = #{inoviceId}")
    List<Product> findProductByID(Integer inoviceId);

    @Select("SELECT * FROM product_tb" +
            " WHERE product_id = #{productId}")
    Product getProductByID(Integer productId);

    @Delete("DELETE FROM product_tb WHERE product_id = #{id}")
    boolean deleteProductByID(@Param("id") Integer productId);

    @Select("INSERT INTO product_tb(product_name,product_price)" +
            " VALUES (#{request.name}, #{request.price}) returning product_id;")
    Integer saveProduct(@Param("request") RequestProduct requestProduct);
    @Select("""
            UPDATE product_tb SET product_name = #{request.name},
                        product_price = #{request.price}productId
                        WHERE product_id = #{productId} returning product_id;
            """)
    Integer updateProduct(@Param("request")RequestProduct requestProduct,Integer productId);
}
