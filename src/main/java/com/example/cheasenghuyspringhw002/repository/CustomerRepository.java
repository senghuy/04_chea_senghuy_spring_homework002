package com.example.cheasenghuyspringhw002.repository;

import com.example.cheasenghuyspringhw002.model.entity.Customer;
import com.example.cheasenghuyspringhw002.model.request.RequestCustomer;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customer_tb WHERE customer_id = #{id}")
    Customer getCustomerByID(Integer customerid);

    @Delete("DELETE FROM customer_tb WHERE customer_id = #{id}")
    boolean deleteCustomerByID(@Param("id") Integer customerid);

    @Select("INSERT INTO customer_tb(customer_name, customer_address, customer_phone)" +
            " VALUES (#{request.name}, #{request.address}, #{request.phone}) returning customer_id;")
    Integer saveCustomer(@Param("request")RequestCustomer requestCustomer);
    @Select("UPDATE customer_tb SET customer_name = #{request.name}," +
            " customer_address = #{request.address}," +
            " customer_phone = #{request.phone} " +
            "WHERE customer_id = #{customerId} returning customer_id")
    Integer updateCustomer(@Param("request")RequestCustomer requestCustomer,Integer customerId);
}
