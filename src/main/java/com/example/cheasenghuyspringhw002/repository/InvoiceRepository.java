package com.example.cheasenghuyspringhw002.repository;

import com.example.cheasenghuyspringhw002.model.entity.Invoice;
import com.example.cheasenghuyspringhw002.model.request.RequestInvoice;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
public interface InvoiceRepository {
//    @Result(property = "invoice_id", column = "invoice_id")
//    @Result(property = "timestamp", column = "invoice_date")
//    @Result(property = "customer",column = "customer_id",
//            one = @One(select="com.example.cheasenghuyspringhw002.repository.CustomerRepository.getCustomerByID"))
//   @Result(property = "products",column ="invoice_id"
//           ,many = @Many(select = "com.example.cheasenghuyspringhw002.repository.ProductRepository.findProductByID"))
    @Select("SELECT * FROM invoice_tb")
    @Results(
            id="invoiceMapper",
            value= {
                    @Result(property = "invoice_id", column = "invoice_id"),
                    @Result(property = "timestamp", column = "invoice_date"),
                    @Result(property = "customer",column = "customer_id",
                            one = @One(select="com.example.cheasenghuyspringhw002.repository.CustomerRepository.getCustomerByID")),
                    @Result(property = "products",column ="invoice_id"
                            ,many = @Many(select = "com.example.cheasenghuyspringhw002.repository.ProductRepository.findProductByID"))
            }
    )
    List<Invoice> findAllInovice();

    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{invoiceId} ")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(Integer invoiceId);

    @Select("""
            INSERT INTO invoice_tb (invoice_date, customer_id) 
            VALUES (#{request.invoice_date} , #{request.customer_id} ) 
            RETURNING invoice_id;
            """)
    Integer savedNewInovice(@Param("request") RequestInvoice requestInvoice);

    @Select("INSERT INTO invoice_detail_tb (invoice_id, product_id) VALUES (#{invoiceId}, #{productId})")
    Integer saveInvoiceByProductId(Integer invoiceId, Integer productId);

    @Select("""
            UPDATE invoice_tb SET invoice_date = #{request.invoice_date} ,customer_id = #{request.customer_id} 
            WHERE invoice_id = #{invoiceId} returning invoice_id;
            """)
    Integer updateInvoiceInvoice(@Param("request") RequestInvoice requestInvoice,Integer invoiceId);

    @Select("""
            DELETE FROM invoice_detail_tb WHERE invoice_id = #{invoiceId}
            """)
    Integer deleteInvoiceInInvoiceDetail(Integer invoiceId);

    @Delete("""
            DELETE FROM invoice_tb WHERE invoice_id = #{invoice_id}
            """)
    boolean deleteInvoiceById(Integer invoice_id);
}
