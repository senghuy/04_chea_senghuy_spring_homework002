package com.example.cheasenghuyspringhw002.model.request;

import com.example.cheasenghuyspringhw002.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestInvoice{
    private Timestamp invoice_date;
    private Integer customer_id;
    private List<Integer> productsIds;
}
